include("diffeq.jl")
using .Ode: linferr, l2err
using LinearAlgebra
using BandedMatrices
using ForwardDiff
using NewtonLineSearch
using Distributions: Uniform
using Plots
using Plots.PlotMeasures
using LaTeXStrings
using PrettyTables
using DelimitedFiles

struct Params
  Nmin::Int
  Nmax::Int
  nwttol::Float64
  type::Symbol
  s0type::Symbol
end

function type2str(type::Symbol, α=nothing)
  if type == :a
    return "a) a(u) = 1 + u^2"
  elseif type == :b
    return "b) a(u) = 1 + (u')^2"
  elseif type == :c
    return "c) a(u) = (1 + (u')^2)^($α)"
  end
end

function s0type2str(s0type::Symbol)
  if s0type == :ones
    return "Startvector s0 = 1"
  elseif s0type == :zeros
    return "Startvector s0 = 0"
  elseif s0type == :rand
    return "Startvector s0 = rand(-1, 1), uniform distribution"
  end
end

function buildnonlindiff(u::Function, type::Symbol, α=nothing)
  if type == :a
    return x -> @. 1 + u(x)^2
  elseif type == :b
    return x -> @. 1 + ForwardDiff.derivative(u, x)^2
  elseif type == :c
    return x -> @. (1 + ForwardDiff.derivative(u, x)^2)^α
  end
end

"""
    discretise1st(f, g, N, scheme)

Discretise u' = f on (0,1) and u(0) = g(0), u(1) = g(1) with forward or
backward differencing scheme.
"""
function discretise1st(g::Function, N::Int, scheme::Symbol)
  h = 1/N
  gridpoints = range(0, 1, length=N+1)

  # discretise f
  # fh = f.(gridpoints[2:end-1])

  # assemble 1st difference operator
  l1 = zeros(N-1)
  if scheme == :forward
    L1 = BandedMatrix{Float64}(undef, (N-1, N-1), (0,1))
    L1[band(1)] .= 1/h
    L1[band(0)] .= -1/h
    l1[end] += g(1)/h
  elseif scheme == :backward
    L1 = BandedMatrix{Float64}(undef, (N-1, N-1), (1,0))
    L1[band(0)] .= 1/h
    L1[band(-1)] .= -1/h
    l1[1] -= g(0)/h
  end
  return L1, l1
end

"""
    arithmeticmean(g, N, scheme)

Vectorise the calculation of the arithmetic mean with matrices, where u(0) =
g(0) and u(1) = g(1).
"""
function arithmeticmean(g::Function, N::Int, scheme::Symbol)
  h = 1/N
  gridpoints = range(0, 1, length=N+1)

  t = zeros(N-1)
  if scheme == :forward
    T = BandedMatrix{Float64}(undef, (N-1, N-1), (0,1))
    T[band(1)] .= 1/2
    T[band(0)] .= 1/2
    t[end] += g(1)/2
  elseif scheme == :backward
    T = BandedMatrix{Float64}(undef, (N-1, N-1), (1,0))
    T[band(0)] .= 1/2
    T[band(-1)] .= 1/2
    t[1] += g(0)/2
  end
  return T, t
end

"""
    testdata(u, a)

Compute a RHS `f` for the 1D non-linear diffusion ODE (a(u)*u')' = f for a
solution `u`.
"""
function testdata(u::Function, a::Function)
  du(x) = ForwardDiff.derivative(u, x)

  return x -> ForwardDiff.derivative(x -> a(x) * du(x), x)
end


function residualerra(u::Function, params::Params)
  Nmin = params.Nmin
  Nmax = params.Nmax
  type = params.type
  s0t = params.s0type

  a = buildnonlindiff(u, type)

  stepsN = floor(Int, log2(Nmax / Nmin)) + 1

  linferrnwt = zeros(stepsN)
  l2errnwt = deepcopy(linferrnwt)
  iternwt = Int.(deepcopy(linferrnwt))

  linferrglb = deepcopy(linferrnwt)
  l2errglb = deepcopy(linferrnwt)
  iterglb = Int.(deepcopy(linferrnwt))

  resnwt = Array{Vector{<:Real}}(undef, stepsN)
  resglb = deepcopy(resnwt)

  stoppingcondition = CompositeCondition(Set([Residual(params.nwttol),
                                              #StepNorm(1e-16),
                                              MaxIterations(100)]))

  Fl = zeros(Nmax - 1)
  DFl = zeros(Nmax - 1, Nmax - 1)

  # don't recreate random vector and other start vectors at every iteration
  if s0t == :ones
    s0tmp = ones(Nmax-1)
  elseif s0t == :zeros
    s0tmp = zeros(Nmax-1)
  elseif s0t == :rand
    s0tmp = rand(Uniform(-1,1), Nmax-1)
  end

  f = testdata(u, a)

  for i in 0:stepsN-1
    N = Nmin * 2^i
    h = 1/N
    gridpoints = range(0, 1, length=N+1)
    # discretise f
    fh = f.(gridpoints[2:end-1])

    F = @view Fl[1:N-1]
    DF = @view DFl[1:N-1, 1:N-1]

    Dp, dp = discretise1st(u, N, :forward)
    Dm, dm = discretise1st(u, N, :backward)
    Tp, tp = arithmeticmean(u, N, :forward)
    Tm, tm = arithmeticmean(u, N, :backward)

    function F!(F, x)
      F .= 1/h .* ((1 .+ (Tp*x .+ tp).^2) .* (Dp*x .+ dp) .-
                   ((1 .+ (Tm*x .+ tm).^2) .* (Dm*x .+ dm))) .- fh
      # println(F)
    end

    function DF!(DF, F, x)
      ForwardDiff.jacobian!(DF, F!, F, x)
    end

    # F(x) = 1/h * ((1 .+ (Tp*x + tp).^2))

    s0 = @view s0tmp[1:N-1]

    tracenwt = newtonmethod(x->F!(F, x), x->DF!(DF, F, x), s0,
                            stoppingcondition)
    traceglb = newtonmethod(x->F!(F, x), x->DF!(DF, F, x), s0,
                            stoppingcondition, lsmethod=Armijo(γ=1e-15))

    unwt = tracenwt[end].x
    uglb = traceglb[end].x

    # Linf error
    linferrnwt[i+1] = linferr(u, unwt, N)
    linferrglb[i+1] = linferr(u, uglb, N)

    # L2 error
    l2errnwt[i+1] = l2err(u, unwt, N)
    l2errglb[i+1] = l2err(u, uglb, N)

    # Newton iterations
    iternwt[i+1] = tracenwt[end].iteration
    iterglb[i+1] = traceglb[end].iteration

    # Residual
    resnwt[i+1] = [state.fnorm for state in tracenwt.states]
    resglb[i+1] = [state.fnorm for state in traceglb.states]
  end
  return linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb, iterglb,
         resglb
end

function residualerrb(u::Function, params::Params)
  Nmin = params.Nmin
  Nmax = params.Nmax
  type = params.type
  s0t = params.s0type

  a = buildnonlindiff(u, type)

  stepsN = floor(Int, log2(Nmax / Nmin)) + 1

  linferrnwt = zeros(stepsN)
  l2errnwt = deepcopy(linferrnwt)
  iternwt = Int.(deepcopy(linferrnwt))

  linferrglb = deepcopy(linferrnwt)
  l2errglb = deepcopy(linferrnwt)
  iterglb = Int.(deepcopy(linferrnwt))

  resnwt = Array{Vector{<:Real}}(undef, stepsN)
  resglb = deepcopy(resnwt)

  stoppingcondition = CompositeCondition(Set([Residual(params.nwttol),
                                              #StepNorm(1e-16),
                                              MaxIterations(100)]))

  Fl = zeros(Nmax - 1)
  DFl = zeros(Nmax - 1, Nmax - 1)

  # don't recreate random vector and other start vectors at every iteration
  if s0t == :ones
    s0tmp = ones(Nmax-1)
  elseif s0t == :zeros
    s0tmp = zeros(Nmax-1)
  elseif s0t == :rand
    s0tmp = rand(Uniform(-1,1), Nmax-1)
  end

  f = testdata(u, a)

  for i in 0:stepsN-1
    N = Nmin * 2^i
    h = 1/N
    gridpoints = range(0, 1, length=N+1)
    # discretise f
    fh = f.(gridpoints[2:end-1])

    F = @view Fl[1:N-1]
    DF = @view DFl[1:N-1, 1:N-1]

    Dp, dp = discretise1st(u, N, :forward)
    Dm, dm = discretise1st(u, N, :backward)

    function F!(F, x)
      F .= 1/h .* ((1 .+ (Dp*x .+ dp).^2) .* (Dp*x .+ dp) .-
                   ((1 .+ (Dm*x .+ dm).^2) .* (Dm*x .+ dm))) .- fh
    end

    function DF!(DF, F, x)
      ForwardDiff.jacobian!(DF, F!, F, x)
    end

    s0 = @view s0tmp[1:N-1]

    tracenwt = newtonmethod(x->F!(F, x), x->DF!(DF, F, x), s0,
                            stoppingcondition)
    traceglb = newtonmethod(x->F!(F, x), x->DF!(DF, F, x), s0,
                            stoppingcondition, lsmethod=Armijo(γ=1e-30))

    unwt = tracenwt[end].x
    uglb = traceglb[end].x

    # Linf error
    linferrnwt[i+1] = linferr(u, unwt, N)
    linferrglb[i+1] = linferr(u, uglb, N)

    # L2 error
    l2errnwt[i+1] = l2err(u, unwt, N)
    l2errglb[i+1] = l2err(u, uglb, N)

    # Newton iterations
    iternwt[i+1] = tracenwt[end].iteration
    iterglb[i+1] = traceglb[end].iteration

    # Residual
    resnwt[i+1] = [state.fnorm for state in tracenwt.states]
    resglb[i+1] = [state.fnorm for state in traceglb.states]
  end
  return linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb, iterglb,
         resglb
end

function residualerrc(u::Function, params::Params, α::Real)
  Nmin = params.Nmin
  Nmax = params.Nmax
  type = params.type
  s0t = params.s0type

  a = buildnonlindiff(u, type, α)

  stepsN = floor(Int, log2(Nmax / Nmin)) + 1

  linferrnwt = zeros(stepsN)
  l2errnwt = deepcopy(linferrnwt)
  iternwt = Int.(deepcopy(linferrnwt))

  linferrglb = deepcopy(linferrnwt)
  l2errglb = deepcopy(linferrnwt)
  iterglb = Int.(deepcopy(linferrnwt))

  resnwt = Array{Vector{<:Real}}(undef, stepsN)
  resglb = deepcopy(resnwt)

  stoppingcondition = CompositeCondition(Set([Residual(params.nwttol),
                                              #StepNorm(1e-16),
                                              MaxIterations(100)]))

  Fl = zeros(Nmax - 1)
  DFl = zeros(Nmax - 1, Nmax - 1)

  # don't recreate random vector and other start vectors at every iteration
  if s0t == :ones
    s0tmp = ones(Nmax-1)
  elseif s0t == :zeros
    s0tmp = zeros(Nmax-1)
  elseif s0t == :rand
    s0tmp = rand(Uniform(-1,1), Nmax-1)
  end

  f = testdata(u, a)

  for i in 0:stepsN-1
    N = Nmin * 2^i
    h = 1/N
    gridpoints = range(0, 1, length=N+1)
    # discretise f
    fh = f.(gridpoints[2:end-1])

    F = @view Fl[1:N-1]
    DF = @view DFl[1:N-1, 1:N-1]

    Dp, dp = discretise1st(u, N, :forward)
    Dm, dm = discretise1st(u, N, :backward)

    function F!(F, x)
      F .= 1/h .* ((1 .+ (Dp*x .+ dp).^2).^α .* (Dp*x .+ dp) .-
                   ((1 .+ (Dm*x .+ dm).^2).^α .* (Dm*x .+ dm))) .- fh
    end

    function DF!(DF, F, x)
      ForwardDiff.jacobian!(DF, F!, F, x)
    end

    s0 = @view s0tmp[1:N-1]

    tracenwt = newtonmethod(x->F!(F, x), x->DF!(DF, F, x), s0,
                            stoppingcondition)
    traceglb = newtonmethod(x->F!(F, x), x->DF!(DF, F, x), s0,
                            stoppingcondition, lsmethod=Armijo(γ=1e-30))

    unwt = tracenwt[end].x
    uglb = traceglb[end].x

    # Linf error
    linferrnwt[i+1] = linferr(u, unwt, N)
    linferrglb[i+1] = linferr(u, uglb, N)

    # L2 error
    l2errnwt[i+1] = l2err(u, unwt, N)
    l2errglb[i+1] = l2err(u, uglb, N)

    # Newton iterations
    iternwt[i+1] = tracenwt[end].iteration
    iterglb[i+1] = traceglb[end].iteration

    # Residual
    resnwt[i+1] = [state.fnorm for state in tracenwt.states]
    resglb[i+1] = [state.fnorm for state in traceglb.states]
  end
  return linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb, iterglb,
         resglb
end

function tablesplot(linferrnwt, l2errnwt, iternwt, resnwt, linferrglb,
                    l2errglb, iterglb, resglb, params::Params,
                    solution::AbstractString, filename::String, α=nothing)
  Nmin = params.Nmin
  Nmax = params.Nmax
  nwttol = params.nwttol
  type = params.type
  s0type = params.s0type

  stepsN = floor(Int, log2(Nmax / Nmin)) + 1

  N = map(i -> Nmin*2^i, 0:stepsN-1)

  header = ["N", "Iterations", "L2-error", "Linf-error"]

  info = "Stoppingcriteria: " *
         "TOL < $(nwttol) (Inf-norm of F) " *
    		 #"or 2-norm of consecutive iterates < 1e-16 " *
         "or 100 Iterations"

  # remove LaTeX-backslashes
  solutiontxt = replace(solution, "\\" => "")

  # open("$filename.txt", "w") do io
  #   println(io, type2str(type, α) * ", " * s0type2str(s0type) * "\n")
  #   println(io, info * "\n" * "-"^length(info) * "\n")

  #   println(io, solutiontxt * "\n" * "#"^length(solutiontxt) * "\n")
  #   println(io, "Newton")
  #   pretty_table(io, Number[N iternwt l2errnwt linferrnwt],
  #                header, formatters=ft_printf("%.4g", [3,4]))

  #   println(io, "\nglobalized Newton")
  #   pretty_table(io, Number[N iterglb l2errglb linferrglb],
  #                header, formatters=ft_printf("%.4g", [3,4]))

  #   println(io)
  # end

  !isdir("$filename") && mkdir("$filename")

  open("$filename/nwt.dat", "w") do io
    println(io, "% " * type2str(type, α) * ", " * s0type2str(s0type) * "\n%")
    println(io, "% " * info * "\n" * "%"^length(info) * "\n%")
    println(io, "% " * solutiontxt * "\n" * "%"^length(solutiontxt) * "\n%")
    println(io, "% Newton")

    for j in 1:stepsN
      println(io, "$(N[j]) & $(iternwt[j]) & $(l2errnwt[j])" *
              " & $(linferrnwt[j])\\\\")
    end
  end

  open("$filename/glb.dat", "w") do io
    println(io, "% " * type2str(type, α) * ", " * s0type2str(s0type) * "\n%")
    println(io, "% " * info * "\n" * "%"^length(info) * "\n%")
    println(io, "% " * solutiontxt * "\n" * "%"^length(solutiontxt) * "\n%")
    println(io, "% globalized Newton")

    for j in 1:stepsN
      println(io, "$(N[j]) & $(iterglb[j]) & $(l2errglb[j])" *
              " & $(linferrglb[j])\\\\")
    end
  end

  # Plot of residual for N=64 and N=1024

  Nind64 = findfirst(==(64), N)
  Nind1024 = findfirst(==(1024), N)

  resnwt64 = resnwt[Nind64]
  resnwt1024 = resnwt[Nind1024]
  resglb64 = resglb[Nind64]
  resglb1024 = resglb[Nind1024]

  # pnwt64 = plot(resnwt64, yscale=:log10,
  #                  xlabel="Iterations", ylabel="Residual", ylims=(1e-16, 1e15),
  #                  title=L"\textrm{Newton, } N = 64", marker=:circle)
  # pglb64 = plot(resglb64, yscale=:log10,
  #                  xlabel="Iterations", ylabel="Residual", ylims=(1e-16, 1e15),
  #                  title=L"\textrm{Globalized Newton, } N = 64", marker=:circle)
  # pnwt1024 = plot(resnwt1024, yscale=:log10,
  #                    xlabel="Iterations", ylabel="Residual", ylims=(1e-16, 1e15),
  #                    title=L"\textrm{Newton, } N = 1024", marker=:circle)
  # pglb1024 = plot(resglb1024, yscale=:log10,
  #                    xlabel="Iterations", ylabel="Residual", ylims=(1e-16, 1e15),
  #                    title=L"\textrm{Globalized Newton, } N = 1024", marker=:circle)

  # p = plot(pnwt64, pglb64, pnwt1024, pglb1024, size=(1500,800),
  #          left_margin=4mm, bottom_margin=4mm)

  # p64 = plot([resnwt64, resglb64], yscale=:log10,
  #            label=["Newton" "globalized Newton"], xlabel="Iterations",
  #            ylabel="Residual", ylims=(1e-16, 1e15), title=L"N = 64",
  #            marker=:circle)
  # p1024 = plot([resnwt1024, resglb1024], yscale=:log10,
  #            label=["Newton" "globalized Newton"], xlabel="Iterations",
  #            ylabel="Residual", ylims=(1e-16, 1e15), title=L"N = 1024",
  #              marker=:circle)

  # p = plot(p64, p1024, size=(1500,400), left_margin=4mm, bottom_margin=4mm)

  # # remove '*'-chars
  # solutionltx = replace(solution, "*" => " ")
  # # workaround for global title (plot_title currently not implemented)
  # title = plot(title=L"\textrm{Residual against Newton iterations for } %$solutionltx",
  #              grid=false, showaxis=false, ticks=false, titlefontsize=30)
  # plot(title, p, layout=@layout [a{0.01h};b])

  # savefig("$filename.pdf")

  writedlm("$filename/nwt64.dat", [0:length(resnwt64)-1 resnwt64])
  writedlm("$filename/nwt1024.dat", [0:length(resnwt1024)-1 resnwt1024])
  writedlm("$filename/glb64.dat", [0:length(resglb64)-1 resglb64])
  writedlm("$filename/glb1024.dat", [0:length(resglb1024)-1 resglb1024])
end

function createtablesplot()
  # a
  cd("nonlindiff/a/")

  params = Params(2^5, 2^10, 1e-9, :a, :zeros)

  ## u(x) = sin(pi*x)
  u(x) = sin(pi*x)
  # start vector s0 = 0
  linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb, iterglb,
  resglb = residualerra(u, params)
  tablesplot(linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb,
             iterglb, resglb, params, raw"u(x) = \sin(\pi*x)", "sinzeros")
  # start vector s0 = rand between -1 and 1
  params = Params(2^5, 2^10, 1e-9, :a, :rand)
  linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb, iterglb,
  resglb = residualerra(u, params)
  tablesplot(linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb,
             iterglb, resglb, params, raw"u(x) = \sin(\pi*x)", "sinrand")

  ## v(x) = sin(10*pi*x)
  v(x) = sin(10*pi*x)
  # start vector s0 = 0
  params = Params(2^5, 2^11, 1e-8, :a, :zeros)
  linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb, iterglb,
  resglb = residualerra(v, params)
  tablesplot(linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb,
             iterglb, resglb, params, raw"u(x) = \sin(10*\pi*x)",
             "sin10zeros")
  # start vector s0 = rand between -1 and 1
  params = Params(2^5, 2^11, 1e-8, :a, :rand)
  linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb, iterglb,
  resglb = residualerra(v, params)
  tablesplot(linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb,
             iterglb, resglb, params, raw"u(x) = \sin(10*\pi*x)",
             "sin10rand")


  # b
  cd("../b/")

  params = Params(2^5, 2^10, 1e-8, :b, :zeros)

  ## u(x) = sin(pi*x)
  u(x) = sin(pi*x)
  # start vector s0 = 0
  linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb, iterglb,
  resglb = residualerrb(u, params)
  tablesplot(linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb,
             iterglb, resglb, params, raw"u(x) = \sin(\pi*x)", "sinzeros")
  # start vector s0 = rand between -1 and 1
  params = Params(2^5, 2^10, 1e-8, :b, :rand)
  linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb, iterglb,
  resglb = residualerrb(u, params)
  tablesplot(linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb,
             iterglb, resglb, params, raw"u(x) = \sin(\pi*x)", "sinrand")


  ## v(x) = sin(10*pi*x)
  v(x) = sin(10*pi*x)
  # start vector s0 = 0
  params = Params(2^5, 2^11, 1e-5, :b, :zeros)
  linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb, iterglb,
  resglb = residualerrb(v, params)
  tablesplot(linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb,
             iterglb, resglb, params, raw"u(x) = \sin(10*\pi*x)",
             "sin10zeros")
  # start vector s0 = rand between -1 and 1
   params = Params(2^5, 2^11, 1e-5, :b, :rand)
  linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb, iterglb,
  resglb = residualerrb(v, params)
  tablesplot(linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb,
             iterglb, resglb, params, raw"u(x) = \sin(10*\pi*x)",
             "sin10rand")


  # c
  cd("../c/")

  α = -1/4
  params = Params(2^5, 2^10, 1e-9, :c, :zeros)

  ## u(x) = sin(pi*x)
  u(x) = sin(pi*x)
  # start vector s0 = 0
  linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb, iterglb,
  resglb = residualerrc(u, params, α)
  tablesplot(linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb,
             iterglb, resglb, params, raw"u(x) = \sin(\pi*x)", "sinzeros", α)
  # start vector s0 = rand between -1 and 1
  params = Params(2^5, 2^10, 1e-9, :c, :rand)
  linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb, iterglb,
  resglb = residualerrc(u, params, α)
  tablesplot(linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb,
             iterglb, resglb, params, raw"u(x) = \sin(\pi*x)", "sinrand", α)


  ## v(x) = sin(10*pi*x)
  v(x) = sin(10*pi*x)
  # start vector s0 = 0
  params = Params(2^5, 2^11, 1e-9, :c, :zeros)
  linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb, iterglb,
  resglb = residualerrc(v, params, α)
  tablesplot(linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb,
             iterglb, resglb, params, raw"u(x) = \sin(10*\pi*x)",
             "sin10zeros", α)
  # start vector s0 = rand between -1 and 1
   params = Params(2^5, 2^11, 1e-9, :c, :rand)
  linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb, iterglb,
  resglb = residualerrc(v, params, α)
  tablesplot(linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb,
             iterglb, resglb, params, raw"u(x) = \sin(10*\pi*x)",
             "sin10rand", α)

  # go back to start diretory (for REPL use)
  cd("../..")
end

function d()
  # d
  cd("nonlindiff/d/")

  α = -0.4
  params = Params(2^5, 2^10, 1e-9, :c, :zeros)

  ## u(x) = sin(pi*x)
  u(x) = sin(pi*x)
  # start vector s0 = 0
  linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb, iterglb,
  resglb = residualerrc(u, params, α)
  tablesplot(linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb,
             iterglb, resglb, params, raw"u(x) = \sin(\pi*x)", "sinzeros", α)
  # start vector s0 = rand between -1 and 1
  params = Params(2^5, 2^10, 1e-9, :c, :rand)
  linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb, iterglb,
  resglb = residualerrc(u, params, α)
  tablesplot(linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb,
             iterglb, resglb, params, raw"u(x) = \sin(\pi*x)", "sinrand", α)


  ## v(x) = sin(10*pi*x)
  v(x) = sin(10*pi*x)
  # start vector s0 = 0
  params = Params(2^5, 2^12, 1e-8, :c, :zeros)
  linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb, iterglb,
  resglb = residualerrc(v, params, α)
  tablesplot(linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb,
             iterglb, resglb, params, raw"u(x) = \sin(10*\pi*x)",
             "sin10zeros", α)
  # start vector s0 = rand between -1 and 1
  params = Params(2^5, 2^12, 1e-8, :c, :rand)
  linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb, iterglb,
  resglb = residualerrc(v, params, α)
  tablesplot(linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb,
             iterglb, resglb, params, raw"u(x) = \sin(10*\pi*x)",
             "sin10rand", α)

  cd("../..")
end


function e()
  # e
  cd("nonlindiff/e/")

  α = -0.49
  params = Params(2^5, 2^10, 1e-9, :c, :zeros)

  ## u(x) = sin(pi*x)
  u(x) = sin(pi*x)
  # start vector s0 = 0
  linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb, iterglb,
  resglb = residualerrc(u, params, α)
  tablesplot(linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb,
             iterglb, resglb, params, raw"u(x) = \sin(\pi*x)", "sinzeros", α)
  # start vector s0 = rand between -1 and 1
  params = Params(2^5, 2^10, 1e-9, :c, :rand)
  linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb, iterglb,
  resglb = residualerrc(u, params, α)
  tablesplot(linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb,
             iterglb, resglb, params, raw"u(x) = \sin(\pi*x)", "sinrand", α)


  ## v(x) = sin(10*pi*x)
  v(x) = sin(10*pi*x)
  # start vector s0 = 0
  params = Params(2^5, 2^12, 1e-8, :c, :zeros)
  linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb, iterglb,
  resglb = residualerrc(v, params, α)
  tablesplot(linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb,
             iterglb, resglb, params, raw"u(x) = \sin(10*\pi*x)",
             "sin10zeros", α)
  # start vector s0 = rand between -1 and 1
  params = Params(2^5, 2^12, 1e-8, :c, :rand)
  linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb, iterglb,
  resglb = residualerrc(v, params, α)
  tablesplot(linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb,
             iterglb, resglb, params, raw"u(x) = \sin(10*\pi*x)",
             "sin10rand", α)

  cd("../..")
end

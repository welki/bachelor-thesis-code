# Bachelor's Thesis Code

This repository contains the results and scripts used for the numerical calculation in
the Bachelor's thesis.

## Structure

The `data` directory contains the raw results and the $`\LaTeX`$-code used for
generating the tables and plots in the thesis. The `nonlindiff` and `ode`
directories correspond to `nonlindiff.jl` and `diffeq.jl` respectively. The `texcode`
directory contains the $`\LaTeX`$-code and data generated from `examples.jl`.

## Usage

The scripts require the directories `nonlindiff/{a,b,c,d,e}`, `ode`, and `texcode` to exist. To create them, you can use the shell script `createdirs.sh`. Note that due to using a random start vector in some calculations, the results will slightly differ on each run.

In order to run the scripts, run Julia in this directory and activate this
environment by entering

```julia
(@v1.5) pkg> activate .
```

in the Pkg REPL. Then, all necessary packages will be downloaded.

### ODE

In order to create the raw results for the ODE, type

```julia
julia> include("diffeq.jl")
julia> Ode.createodetablesplot()
```

The data will be placed in `ode/`.

### Non-linear diffusion equation

In order to create the raw results for the non-linear diffusion equation, type

```julia
julia> include("nonlindiff.jl")
julia> createtablesplot()  # for a, b, c
julia> d(); e()
```

The date will be placed in `nonlindiff/`. Note that it can take very long to finish.

## Examples

For running `example.jl`, it is sufficient to enter

```julia
julia> include("examples.jl")
```

The results can then be found in `texcode/`.

module Ode

using LinearAlgebra
using BandedMatrices
using ForwardDiff
using NewtonLineSearch
# using Latexify
# using DataFrames
using Distributions: Uniform
using Plots
using Plots.PlotMeasures
using LaTeXStrings
using PrettyTables
using DelimitedFiles
using Latexify


struct Params{T<:Real}
  Nmin::Int
  Nmax::Int
  εmin::T
  εmax::T
end

"""
    discretisepoisson(f, g, N)

Discretise Poisson's equation in 1D:
 -εu'' = f on (0,1) and u(0) = g(0), u(1) = g(1)
"""
function discretisepoisson(f::Function, g::Function, N::Int, ε)
  h = 1/N
  gridpoints = range(0, 1, length=N+1)
  # assemble 2nd difference operator
  L2 = BandedMatrix{Float64}(undef, (N-1, N-1), (1,1))
  L2[band(0)] .= -2.0/h^2
  L2[band(1)] .= L2[band(-1)] .= 1.0/h^2

  # assemble system matrix
  A = -ε*L2

  # discretise f
  fh = f.(gridpoints[2:end-1])

  # assemble RHS
  b = fh
  b[1] += ε*g(0)/h^2
  b[end] += ε*g(1)/h^2

  return A, b
end


"""
    testdata(u, ε)

Compute a RHS `f` for the 1D-DE -εu'' + u^3 = f for a solution `u`
"""
function testdata(u::Function, ε)
  # 1st and 2nd derivative of u
  du(x) = ForwardDiff.derivative(u, x)
  d2u(x) = ForwardDiff.derivative(du, x)

  # assemble RHS
  return x -> -ε*d2u(x) + u(x)^3
end


function linferr(u::Function, uh::AbstractVector{<:Real}, N::Int)
  gridpoints = range(0, 1, length=N+1)
  # at boundary error is zero by construction
  err = u.(gridpoints[2:end-1]) - uh
  return norm(err, Inf)
end

function l2err(u::Function, uh::AbstractVector{<:Real}, N::Int)
  gridpoints = range(0, 1, length=N+1)
  h = 1/N
  # at boundary error is zero by construction
  err = u.(gridpoints[2:end-1]) - uh
  return norm(err, 2) * sqrt(h)
end


function oderesidualerr(u::Function, params::Params, s0t::Symbol)
  Nmin = params.Nmin
  Nmax = params.Nmax
  εmin = params.εmin
  εmax = params.εmax

  stepsN = floor(Int, log2(Nmax / Nmin)) + 1
  stepsε = floor(Int, log10(εmax / εmin)) ÷ 2 + 1

  linferrnwt = zeros(stepsN, stepsε)
  l2errnwt = deepcopy(linferrnwt)
  iternwt = Int.(deepcopy(linferrnwt))

  linferrglb = deepcopy(linferrnwt)
  l2errglb = deepcopy(linferrnwt)
  iterglb = Int.(deepcopy(linferrnwt))

  resnwt = Array{Vector{<:Real}}(undef, stepsN, stepsε)
  resglb = deepcopy(resnwt)

  stoppingcondition = CompositeCondition(Set([Residual(1e-9),
                                              #StepNorm(1e-16),
                                              MaxIterations(100)]))

  Fl = zeros(Nmax - 1)
  DFl = zeros(Nmax - 1, Nmax - 1)

  # don't recreate random vector and other start vectors at every iteration
  if s0t == :ones
    s0tmp = ones(Nmax-1)
  elseif s0t == :zeros
    s0tmp = zeros(Nmax-1)
  elseif s0t == :rand
    s0tmp = rand(Uniform(-1,1), Nmax-1)
  end

  for j in 0:stepsε-1
    ε = εmin * 10^(2*j)
    f = testdata(u, ε)
    for i in 0:stepsN-1
      N = Nmin * 2^i
      A, b = discretisepoisson(f, u, N, ε)

      F = @view Fl[1:N-1]
      DF = @view DFl[1:N-1, 1:N-1]

      function F!(F, x)
        mul!(F, A, x)
        F .+= x.^3 .- b
      end

      function DF!(DF, F, x)
        ForwardDiff.jacobian!(DF, F!, F, x)
      end

      s0 = @view s0tmp[1:N-1]

      tracenwt = newtonmethod(x->F!(F, x), x->DF!(DF, F, x), s0,
                              stoppingcondition)
      traceglb = newtonmethod(x->F!(F, x), x->DF!(DF, F, x), s0,
                              stoppingcondition, lsmethod=Armijo(γ=1e-14))

      unwt = tracenwt[end].x
      uglb = traceglb[end].x

      # Linf error
      linferrnwt[i+1, j+1] = linferr(u, unwt, N)
      linferrglb[i+1, j+1] = linferr(u, uglb, N)

      # L2 error
      l2errnwt[i+1, j+1] = l2err(u, unwt, N)
      l2errglb[i+1, j+1] = l2err(u, uglb, N)

      # Newton iterations
      iternwt[i+1, j+1] = tracenwt[end].iteration
      iterglb[i+1, j+1] = traceglb[end].iteration

      # Residual
      resnwt[i+1, j+1] = [state.fnorm for state in tracenwt.states]
      resglb[i+1, j+1] = [state.fnorm for state in traceglb.states]
    end
  end
  return linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb, iterglb,
  			 resglb
end

function odetablesplot(linferrnwt, l2errnwt, iternwt, resnwt, linferrglb,
                       l2errglb, iterglb, resglb, params::Params,
                       solution::AbstractString, filename::String)
  Nmin = params.Nmin
  Nmax = params.Nmax
  εmin = params.εmin
  εmax = params.εmax

  stepsN = floor(Int, log2(Nmax / Nmin)) + 1
  stepsε = floor(Int, log10(εmax / εmin)) ÷ 2 + 1

  N = map(i -> Nmin*2^i, 0:stepsN-1)
  ε = map(i -> εmin*10^(2*i), 0:stepsε-1)

  header = ["N", "Iterations", "L2-error", "Linf-error"]

  info = "Stoppingcriteria: " *
         "TOL < 1e-9 (Inf-norm of F) " *
    		 #"or 2-norm of consecutive iterates < 1e-16 " *
         "or 100 Iterations"

  # remove LaTeX-backslashes
  solutiontxt = replace(solution, "\\" => "")

  # open("$filename.txt", "w") do io
  #   println(io, info * "\n" * "-"^length(info) * "\n")
  #   for i in 1:stepsε
  #     println(io, solutiontxt * "\n" * "#"^length(solutiontxt) * "\n")
  #     println(io, "ε = $(ε[i]), Newton")
  #     pretty_table(io, Number[N iternwt[:,i] l2errnwt[:,i] linferrnwt[:,i]],
  #                  header, formatters=ft_printf("%.4g", [3,4]), backend=:latex)

  #     println(io, "\nε = $(ε[i]), globalized Newton")
  #     pretty_table(io, Number[N iterglb[:,i] l2errglb[:,i] linferrglb[:,i]],
  #     header, formatters=ft_printf("%.4g", [3,4]), backend=:latex)

  #     println(io)
  #   end
  # end

  !isdir("$filename") && mkdir("$filename")

  for i in 1:stepsε
    open("$filename/nwteps$i.dat", "w") do io
      println(io, "% " * info * "\n"  * "%"^length(info) * "\n%")
      println(io, "% " * solutiontxt * "\n" * "%"^length(solutiontxt) * "\n%")
      println(io, "% ε = $(ε[i]), Newton")
      # print(io, latexify(Number[N iternwt[:,i] l2errnwt[:,i] linferrnwt[:,i]],
      #                    env=:table, latex=false, booktabs=true))
      for j in 1:stepsN
        println(io, "$(N[j]) & $(iternwt[j,i]) & $(l2errnwt[j,i])" *
                " & $(linferrnwt[j,i])\\\\")
      end
    end

    open("$filename/glbeps$i.dat", "w") do io
      println(io, "% " * info * "\n"  * "%"^length(info) * "\n%")
      println(io, "% " * solutiontxt * "\n" * "%"^length(solutiontxt) * "\n%")
      println(io, "% ε = $(ε[i]), globalized Newton")
      for j in 1:stepsN
        println(io, "$(N[j]) & $(iterglb[j,i]) & $(l2errglb[j,i])" *
                " & $(linferrglb[j,i])\\\\")
      end
    end
  end

  # Plot of residual for N=64 and N=1024

  Nind64 = findfirst(==(64), N)
  Nind1024 = findfirst(==(1024), N)

  resnwt64 = resnwt[Nind64, :]
  resnwt1024 = resnwt[Nind1024, :]
  resglb64 = resglb[Nind64, :]
  resglb1024 = resglb[Nind1024, :]

  # pnwt64 = plot(resnwt64, yscale=:log10, label="ε = " .* string.(ε'),
  #                  xlabel="Iterations", ylabel="Residual", ylims=(1e-16, 1e15),
  #                  title=L"\textrm{Newton, } N = 64", marker=:circle)
  # pglb64 = plot(resglb64, yscale=:log10, label="ε = " .* string.(ε'),
  #                  xlabel="Iterations", ylabel="Residual", ylims=(1e-16, 1e15),
  #                  title=L"\textrm{Globalized Newton, } N = 64", marker=:circle)
  # pnwt1024 = plot(resnwt1024, yscale=:log10, label="ε = " .* string.(ε'),
  #                    xlabel="Iterations", ylabel="Residual", ylims=(1e-16, 1e15),
  #                    title=L"\textrm{Newton, } N = 1024", marker=:circle)
  # pglb1024 = plot(resglb1024, yscale=:log10, label="ε = " .* string.(ε'),
  #                    xlabel="Iterations", ylabel="Residual", ylims=(1e-16, 1e15),
  #                    title=L"\textrm{Globalized Newton, } N = 1024", marker=:circle)

  # p = plot(pnwt64, pglb64, pnwt1024, pglb1024, size=(400,400),
  #          left_margin=4mm, bottom_margin=4mm)

  # # remove '*'-chars
  # solutionltx = replace(solution, "*" => " ")
  # workaround for global title (plot_title currently not implemented)
  # title = plot(title=L"\textrm{Residual against Newton iterations for } %$solutionltx",
  #             grid=false, showaxis=false, ticks=false, titlefontsize=30)
  # plot(title, p, layout=@layout [a{0.01h};b])

  # plot(p)

  # savefig("$filename.tex")

  for i in 1:stepsε
    writedlm("$filename/nwt64eps$i.dat", [0:length(resnwt64[i])-1 resnwt64[i]])
    writedlm("$filename/nwt1024eps$i.dat", [0:length(resnwt1024[i])-1 resnwt1024[i]])
    writedlm("$filename/glb64eps$i.dat", [0:length(resglb64[i])-1 resglb64[i]])
    writedlm("$filename/glb1024eps$i.dat", [0:length(resglb1024[i])-1 resglb1024[i]])
  end
end

function createodetablesplot()
  cd("ode/")

  # pgfplotsx()

  params = Params(2^5, 2^10, 1e-6, 1.0)

  ## u(x) = sin(pi*x)
  u(x) = sin(pi*x)
  # start vector s0 = 0
  linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb, iterglb,
  resglb = oderesidualerr(u, params, :zeros)
  odetablesplot(linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb,
                iterglb, resglb, params, raw"u(x) = \sin(\pi*x)", "sinzeros")
  # start vector s0 = rand between -1 and 1
  linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb, iterglb,
  resglb = oderesidualerr(u, params, :rand)
  odetablesplot(linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb,
                iterglb, resglb, params, raw"u(x) = \sin(\pi*x)", "sinrand")

  ## v(x) = sin(10*pi*x)
  v(x) = sin(10*pi*x)
  # start vector s0 = 0
  linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb, iterglb,
  resglb = oderesidualerr(v, params, :zeros)
  odetablesplot(linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb,
                iterglb, resglb, params, raw"u(x) = \sin(10*\pi*x)",
                "sin10zeros")
  # start vector s0 = rand between -1 and 1
  linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb, iterglb,
  resglb = oderesidualerr(v, params, :rand)
  odetablesplot(linferrnwt, l2errnwt, iternwt, resnwt, linferrglb, l2errglb,
                iterglb, resglb, params, raw"u(x) = \sin(10*\pi*x)",
                "sin10rand")

  # go back to start directory (for REPL use)
  cd("..")
end

end

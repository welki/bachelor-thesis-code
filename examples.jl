using NewtonLineSearch
using Latexify

F(x) = atan(x)
DF(x) = 1 / (1 + x^2)

# Newton converges
trconv = newtonmethod(F, DF, 1.0, MaxIterations(3))

# Newton diverges
trdiv = newtonmethod(F, DF, 5.0, MaxIterations(2))

# Armijo
trarm = newtonmethod(F, DF, 5.0, MaxIterations(4), lsmethod=Armijo())

# write to file
const DIR = "texcode/"

open(joinpath(DIR, "newtconv.tex"), "w") do io
  print(io, latexify(trconv))
end

open(joinpath(DIR, "newtdiv.tex"), "w") do io
  print(io, latexify(trdiv))
end

open(joinpath(DIR, "armijo.tex"), "w") do io
  print(io, latexify(trarm))
end

# write out x values for plot

open(joinpath(DIR, "xconv.txt"), "w") do io
  xconv = [state.x for state in trconv.states]
  print(io, join(xconv, ","))
end

open(joinpath(DIR, "xdiv.txt"), "w") do io
  xdiv = [state.x for state in trdiv.states]
  print(io, join(xdiv, ","))
end

open(joinpath(DIR, "xarmijo.txt"), "w") do io
  xarm = [state.x for state in trarm.states]
  print(io, join(xarm, ","))
end
